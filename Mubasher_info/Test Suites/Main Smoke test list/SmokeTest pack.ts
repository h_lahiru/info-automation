<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>SmokeTest pack</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>0811b8ab-a554-44dd-8438-e4eedac3e8db</testSuiteGuid>
   <testCaseLink>
      <guid>4723104c-32a0-4e43-a7f5-0b55218dc2bf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/Login to Mubasher with correct credentials</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c6689edc-aaf2-4517-afde-9485d9ebdfb7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Signup/Create a new user</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>62588bdf-770e-4688-b67c-46ab6dc374bd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/forgot-password</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>db4368e5-af65-4cbc-82ec-5dbe45ea5bdb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/Login to Muasher with incorrect credintials</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a1d6aa02-5a0c-4d4b-8e33-9d1ed9a96716</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Market status Check/Verification of market and it status</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>246201c9-e818-41e0-988f-01bdd28004e1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke Test/info smoke test</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4e7e4c3a-dd8b-46ab-aa63-1e21b6d4097b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke Test/Latest News Check</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bcf20b62-cba9-4d5e-ac96-09211ad20a6a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke Test/Market Date Check</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e465f8f9-7b37-45c8-83e6-48e2bf2af3d3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke Test/Personal Functions</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8f968f8d-53bf-4988-b054-88465a0fcdb7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke Test/Stock Prices Check</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>af42c3fd-6515-436c-b63d-5df9313d31a1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke Test/Stock Prices With each market</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d86bb66b-c359-4484-b2a8-3f008375cd5b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke Test/3rdparty sites</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
