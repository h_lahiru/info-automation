<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Market Name</name>
   <tag></tag>
   <elementGuidId>6df4ad6b-b385-4f48-a471-20702653d4e2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//h1[@class='mi-section__title']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//h1[@class='mi-section__title']</value>
   </webElementProperties>
</WebElementEntity>
