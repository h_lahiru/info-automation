<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>My Alerts</name>
   <tag></tag>
   <elementGuidId>1c1d0bde-31ff-4a70-8c4d-d80bb73ed7d1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//a[contains(text(),'My Alerts')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//a[contains(text(),'My Alerts')]</value>
   </webElementProperties>
</WebElementEntity>
