<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>stock prices last update</name>
   <tag></tag>
   <elementGuidId>df0ad800-6b3b-4372-8040-6a5304f447dd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//span[@class='stock-prices__last-update ng-binding']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//span[@class='stock-prices__last-update ng-binding']</value>
   </webElementProperties>
</WebElementEntity>
