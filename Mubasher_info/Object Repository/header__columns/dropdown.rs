<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>dropdown</name>
   <tag></tag>
   <elementGuidId>6537db94-a612-4939-ad6d-51368bd2e1b1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//nav[@class='header__countries-menu--desktop mi-js-countries-menu-desktop']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//nav[@class='header__countries-menu--desktop mi-js-countries-menu-desktop']</value>
   </webElementProperties>
</WebElementEntity>
