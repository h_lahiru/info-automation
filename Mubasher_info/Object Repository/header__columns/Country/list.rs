<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>list</name>
   <tag></tag>
   <elementGuidId>211aecb0-31f6-4b49-9403-68579a2c4af6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//nav[@class='header__countries-menu--desktop mi-js-countries-menu-desktop']//ul</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//nav[@class='header__countries-menu--desktop mi-js-countries-menu-desktop']//ul</value>
   </webElementProperties>
</WebElementEntity>
