<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>welcome</name>
   <tag></tag>
   <elementGuidId>c563bfd6-35ec-40ca-a83e-c5d440fd8129</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>body.en.ng-scope:nth-child(2) div.js-wrapper.mm-page.mm-slideout:nth-child(3) header.header:nth-child(1) div.header-desktop.mi-show-for-large-up div.header__top-for-desktop div.header__top-for-desktop-row > a.header__user-name:nth-child(17)</value>
      </entry>
      <entry>
         <key>XPATH</key>
      </entry>
      <entry>
         <key>CSS</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>css</name>
      <type>Main</type>
      <value>body.en.ng-scope:nth-child(2) div.js-wrapper.mm-page.mm-slideout:nth-child(3) header.header:nth-child(1) div.header-desktop.mi-show-for-large-up div.header__top-for-desktop div.header__top-for-desktop-row > a.header__user-name:nth-child(17)</value>
   </webElementProperties>
</WebElementEntity>
