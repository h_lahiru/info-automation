<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>login button</name>
   <tag></tag>
   <elementGuidId>962c1fb3-4923-4b87-a320-d740a25212b5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//a[@class='header__login-button'][contains(text(),'Login')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//a[@class='header__login-button'][contains(text(),'Login')]</value>
   </webElementProperties>
</WebElementEntity>
