<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>1st investor</name>
   <tag></tag>
   <elementGuidId>a3cb10f8-8d6d-4aca-8bfd-42a70f277348</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//a[@class='header__login-button'][contains(text(),'1st Investor')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//a[@class='header__login-button'][contains(text(),'1st Investor')]</value>
   </webElementProperties>
</WebElementEntity>
