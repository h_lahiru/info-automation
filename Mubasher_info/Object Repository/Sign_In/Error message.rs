<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Error message</name>
   <tag></tag>
   <elementGuidId>8f870ff1-6d1a-489d-ab1a-88f677d10816</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//small[@class='error']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//small[@class='error']</value>
   </webElementProperties>
</WebElementEntity>
