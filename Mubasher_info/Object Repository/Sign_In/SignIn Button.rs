<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>SignIn Button</name>
   <tag></tag>
   <elementGuidId>228ec741-add6-40c2-844c-d0b60ad8cf2e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@name = 'submit']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>submit</value>
   </webElementProperties>
</WebElementEntity>
