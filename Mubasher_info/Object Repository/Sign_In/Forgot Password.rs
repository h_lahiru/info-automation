<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Forgot Password</name>
   <tag></tag>
   <elementGuidId>7fb4c803-e592-4d9a-8609-f4109225507a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//a[contains(text(),'Forgot Password')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//a[contains(text(),'Forgot Password')]</value>
   </webElementProperties>
</WebElementEntity>
