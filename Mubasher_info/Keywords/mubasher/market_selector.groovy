package mubasher

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

public class market_selector {


	private String market_selector_selector(String dropID){
		return '//*[@id="wrapper"]/div[1]/header/div[2]/div[2]/div[1]/div[1]/div[2]/nav/ul/li[' + dropID + ']/a';
	}

	private TestObject getdropMenuTestObject(String dropID){
		TestObject dropitem = new TestObject(dropID);
		dropitem.addProperty("xpath", ConditionType.EQUALS, market_selector_selector(dropID), true);
		return dropitem;
	}

	@Keyword
	public void navigatetoMaindropMenuItem(String dropID){
		TestObject dropitem = getdropMenuTestObject(dropID);
		WebUI.waitForElementPresent(dropitem,GlobalVariable.load_time);//(latest)
		WebUI.verifyElementPresent(dropitem, GlobalVariable.load_time, FailureHandling.CONTINUE_ON_FAILURE);
		WebUI.scrollToElement(dropitem,GlobalVariable.load_time);
		WebUI.doubleClick(dropitem);
	}
}

