package mubasher

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

public class header__navigation {

	private String market_selector_selector(String nav_id){
		return '//*[@id="wrapper"]/div[1]/header/div[2]/nav/ul/li[' + nav_id + ']/a'
	}

	private TestObject getHeadernavMenuTestObject(String nav_id){
		TestObject navitem = new TestObject(nav_id)
		navitem.addProperty("xpath", ConditionType.EQUALS, market_selector_selector(nav_id), true)
		return navitem
	}

	@Keyword
	public void getHeadernavMenuTestObjectItem(String nav_id){
		TestObject navitem = getHeadernavMenuTestObject(nav_id);
		WebUI.waitForElementPresent(navitem,GlobalVariable.load_time)
		WebUI.verifyElementPresent(navitem, GlobalVariable.load_time, FailureHandling.CONTINUE_ON_FAILURE);
		WebUI.focus(navitem)
		WebUI.click(navitem)
	}
}


//WebUI.waitForElementPresent(navitem,GlobalVariable.load_time)
//WebUI.verifyElementPresent(navitem, GlobalVariable.load_time, FailureHandling.CONTINUE_ON_FAILURE);
//WebUI.focus(navitem)