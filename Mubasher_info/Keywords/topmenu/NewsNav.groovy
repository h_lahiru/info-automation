package topmenu

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.jcraft.jzlib.Inflate.Return
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable

public class NewsNav {

	private String getMainMenuXpath(String menuid1,String menuid2,String menuid3){

		return "//*[@id='news']/div/ul/li[" + menuid1 + "]/div/ul[" + menuid2 + "]/li[" + menuid3 + "]/a"
	}

	private TestObject getMainMenuTestObjectNews(String menuid1,String menuid2,String menuid3){
		String abcd = getMainMenuXpath(menuid1, menuid2, menuid3);
		TestObject menuitem = new TestObject(abcd)
		menuitem.addProperty("xpath", ConditionType.CONTAINS,abcd , true)
		return menuitem
	}

	@Keyword
	public void navigatetoSubMenuItemNews(String menuid1,String menuid2,String menuid3){
		TestObject menuitem = getMainMenuTestObjectNews(menuid1, menuid2, menuid3);
		//		CustomKeywords.'mubasher.header__navigation.getHeadernavMenuTestObjectItem'('2');
		WebUI.waitForElementVisible(menuitem,GlobalVariable.load_time)
		WebUI.waitForElementPresent(menuitem,GlobalVariable.load_time)
		WebUI.verifyElementPresent(menuitem, GlobalVariable.load_time, FailureHandling.CONTINUE_ON_FAILURE);
		WebUI.focus(menuitem)
		WebUI.doubleClick(menuitem)
		//WebUI.delay(1)
	}
}


//WebUI.waitForElementPresent(navitem,GlobalVariable.load_time)
//WebUI.verifyElementPresent(navitem, GlobalVariable.load_time, FailureHandling.CONTINUE_ON_FAILURE);
//WebUI.focus(navitem)