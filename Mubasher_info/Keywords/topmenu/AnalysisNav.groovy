package topmenu

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

public class AnalysisNav {
	private String getMainMenuXpath(String menuid1,String menuid2){

		return "//*[@id='analysis-tools']/div/div[" + menuid1 + "]/ul/li[" + menuid2 + "]/a"
	}

	private TestObject getMainMenuTestObjectAnalysis(String menuid1,String menuid2){
		String abc = getMainMenuXpath(menuid1, menuid2);
		TestObject menuitem = new TestObject(abc)
		menuitem.addProperty("xpath", ConditionType.CONTAINS,abc, true)
		return menuitem
	}

	@Keyword
	public void navigatetoSubMenuItemAnalysis(String menuid1,String menuid2){
		TestObject menuitem = getMainMenuTestObjectAnalysis(menuid1, menuid2);

		WebUI.waitForElementVisible(menuitem,GlobalVariable.load_time)
		WebUI.waitForElementPresent(menuitem,GlobalVariable.load_time)
		WebUI.verifyElementPresent(menuitem, GlobalVariable.load_time, FailureHandling.CONTINUE_ON_FAILURE);
		WebUI.focus(menuitem)
		WebUI.click(menuitem)
		//WebUI.delay(1)
	}
}


//WebUI.waitForElementPresent(navitem,GlobalVariable.load_time)
//WebUI.verifyElementPresent(navitem, GlobalVariable.load_time, FailureHandling.CONTINUE_ON_FAILURE);
//WebUI.focus(navitem)