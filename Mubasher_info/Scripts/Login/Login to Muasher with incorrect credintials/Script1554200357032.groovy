import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.logging.KeywordLogger
WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.URl)

WebUI.maximizeWindow()

//change the language
WebUI.click(findTestObject('header__top-for-desktop/English'))

//navigate to login button
WebUI.click(findTestObject('header__top-for-desktop/login button'))

//sending un/pw  from a excel sheet
WebUI.sendKeys(findTestObject('Sign_In/Email'), findTestData('Data file/Mubasher data').getValue(1, 1))

WebUI.sendKeys(findTestObject('Sign_In/Password'), findTestData('Data file/Mubasher data').getValue(1, 1))

//click on Sign in
WebUI.click(findTestObject('Sign_In/SignIn Button'))


// when Incorrect un/pw entered
def text = WebUI.getText(findTestObject('Sign_In/Error message'))

text.contains('Incorrect Username or Password')

println('you have entered incorrect user name or password(test pass)')

KeywordLogger log = new KeywordLogger()
log.logInfo("you have entered incorrect user name or password(test pass)")

//lahiru 02/04/2019

