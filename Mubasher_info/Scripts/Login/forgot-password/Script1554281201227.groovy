import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

//change the language
CustomKeywords.'mubasher.help_keyword_start.gettingstart'()

//navigate to login button
WebUI.click(findTestObject('header__top-for-desktop/login button'))

//click on Fogot password button
WebUI.click(findTestObject('Sign_In/Forgot Password'))

//sending a new email or exsisting email to email text field
WebUI.sendKeys(findTestObject('Sign_In/Retrieve Password email'), findTestData('Data file/Mubasher data').getValue(1, 1))

//click on Submit
WebUI.click(findTestObject("Sign_In/Retrieve Password Submit"))

WebUI.verifyTextPresent('Reset Password', false, FailureHandling.OPTIONAL)

// lahiru 03/04/2019


