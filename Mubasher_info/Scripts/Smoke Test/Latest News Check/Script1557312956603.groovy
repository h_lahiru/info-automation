import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import groovy.time.TimeCategory
import java.time.format.DateTimeFormatter;
import java.time.LocalDateTime;
import java.awt.print.Printable
import java.text.SimpleDateFormat;
import java.util.Date;
import com.kms.katalon.core.logging.KeywordLogger

//loging into the mubasher web app 
CustomKeywords.'mubasher.help_keyword_start.gettingstart'()

KeywordLogger log = new KeywordLogger()

//navigate to News >> Latest News
for(int i = 3;i<=3;i++){
	//navigate to news 1st column
	CustomKeywords.'mubasher.header__navigation.getHeadernavMenuTestObjectItem'('3')
	CustomKeywords.'topmenu.NewsNav.navigatetoSubMenuItemNews'('1','1',Integer.toString(i))
}

//modde changing to "List mode"
WebUI.click(findTestObject("News/newsMode"))

WebUI.click(findTestObject("News/1stLatest"))

WebUI.scrollToElement(findTestObject("News/DateTimeLocation"), GlobalVariable.load_time)

//news date 
String copieddate = WebUI.getText(findTestObject("News/DateTimeObject"))

//system date generating 
DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd MMM yyyy hh:mm a");//yyyy/MM/dd HH:mm:ss
LocalDateTime now = LocalDateTime.now();

//Ksa time
todayDate=dtf.format(now.minusHours(2).minusMinutes(30));

SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy hh:mm a");
Date date1 = sdf.parse(todayDate);
Date date2 = sdf.parse(copieddate);



System.out.println("  Today : " + sdf.format(date1));
System.out.println("  Latest news date/time : " + sdf.format(date2));


//long diff = date1.getTime() - date2.getTime();
//time gap in minutes (system date - news date)
int ab = ((date1.getTime()-date2.getTime())/60000);

//geting number of hours
float y = ab/60


println ("  last updated :  " + y +" H || " + ab + " Min ago")

log.logInfo("  Today : " + sdf.format(date1))

log.logInfo("  Latest news date/time : " + sdf.format(date2))

log.logInfo("  last updated :  " + y +" H || " + ab + " Min ago")

//System.out.print(ab + "  minutes ");

WebUI.verifyGreaterThanOrEqual(60,ab)

//if(ab<=2){
//	
//	print "news are upto date"
//}


//if (date1.compareTo(date2) > 0) {
//	System.out.println("Date1 is after Date2");
//} else if (date1.compareTo(date2) < 0) {
//	System.out.println("Date1 is before Date2");
//} else if (date1.compareTo(date2) == 0) {
//	System.out.println("Date1 is equal to Date2");
//} else {
//	System.out.println("How to get here?");
//}


/*

SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
Date date1 = sdf.parse("2009-12-31");
Date date2 = sdf.parse("2010-01-31");

System.out.println("date1 : " + sdf.format(date1));
System.out.println("date2 : " + sdf.format(date2));

if (date1.compareTo(date2) > 0) {
	System.out.println("Date1 is after Date2");
} else if (date1.compareTo(date2) < 0) {
	System.out.println("Date1 is before Date2");
} else if (date1.compareTo(date2) == 0) {
	System.out.println("Date1 is equal to Date2");
} else {
	System.out.println("How to get here?");
}

*/


//10-05-2019 lahiru Madhawa