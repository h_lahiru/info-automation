import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import com.kms.katalon.core.logging.KeywordLogger
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI


KeywordLogger log = new KeywordLogger()

//loging into the mubasher web app
CustomKeywords.'mubasher.help_keyword_start.gettingstart'()

//navigate to News >> Latest News
			for(int i =3;i<=3;i++){
				//navigate to news 1st column
				CustomKeywords.'mubasher.header__navigation.getHeadernavMenuTestObjectItem'('4')
				CustomKeywords.'topmenu.companiesNav.navigatetoSubMenuItemCompanies'('1', Integer.toString(i))
			}
			
String stockLastUpdateday = WebUI.getText(findTestObject("Companies/stock prices last update"))

stockLastUpdateday=stockLastUpdateday.split(": ")[1];

println stockLastUpdateday;

//system date generating
DateTimeFormatter dtf = DateTimeFormatter.ofPattern("EEEE dd MMM yyyy - hh:mm a");//yyyy/MM/dd HH:mm:ss
LocalDateTime now = LocalDateTime.now();

//Ksa time
todayDate=dtf.format(now.minusHours(2).minusMinutes(30));

SimpleDateFormat sdf = new SimpleDateFormat("EEEE dd MMM yyyy - hh:mm a");// EEEE dd MMM yyyy - hh:mm a
Date date1 = sdf.parse(todayDate);
Date date2 = sdf.parse(stockLastUpdateday);


System.out.println("Now date/time : " + sdf.format(date1));
System.out.println("Stock Price date/time : " + sdf.format(date2));

int ab = ((date1.getTime()-date2.getTime())/60000);

//geting number of hours
float y = ab/60

if (y >= 0) {
println ("  last updated :  " + y +" H || " + ab + " Min ago")
log.logInfo("Now date/time : " + sdf.format(date1))
log.logInfo("Stock Price date/time : " + sdf.format(date2))
log.logInfo("  last updated :  " + y +" H || " + ab + " Min ago")

//System.out.print(ab + "  minutes ");



WebUI.verifyGreaterThanOrEqual(30,ab)

//Lahiru Madhawa - 13/05/2019
} else {
// it's a negative
println ("  last updated :  " + y +" H || " + ab + " \u001b[1;31mMin\u001b[0m ago")
log.logError('check your dates, there are something wrong ')
}

