import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.logging.KeywordLogger

CustomKeywords.'mubasher.help_keyword_start.gettingstart'();

WebUI.click(findTestObject("header__top-for-desktop/Decypha"));

WebUI.switchToWindowIndex(1)

WebUI.verifyTextPresent("DECYPHA PACKAGES", false)

WebUI.click(findTestObject("header__top-for-desktop/1st investor section/1st investor"));

WebUI.switchToWindowIndex(2)

WebUI.waitForPageLoad(GlobalVariable.load_time)

WebUI.click(findTestObject("header__top-for-desktop/1st investor section/trade"))

WebUI.switchToWindowIndex(1)

WebUI.click(findTestObject("header__top-for-desktop/1st investor section/Pulse"))

String abc = "1st investor / pulse / decypha pages loaded"

println abc

KeywordLogger log = new KeywordLogger()
log.logInfo(abc);



//WebUI.verifyTextPresent("WELCOME TO THE", false)

//WebUI.verifyOptionPresentByLabel(findTestObject("header__top-for-desktop/1st investor section/lable"), "1st INVESTOR", false, 0, FailureHandling.STOP_ON_FAILURE)
