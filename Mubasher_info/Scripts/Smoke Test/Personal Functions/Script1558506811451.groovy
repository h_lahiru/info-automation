import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

CustomKeywords.'mubasher.help_keyword_start.gettingstart'()

for (int i = 1; i <= 3; i++) {
    //navigate to news 1st column with user name and pw 
	
	CustomKeywords.'mubasher.header__navigation.getHeadernavMenuTestObjectItem'('10')

  
  CustomKeywords.'topmenu.personalNav.navigatetoSubMenuItempersonalNav'(Integer.toString(i))
  
  
  //Navigate to My Alerts , Alerts and  Create an Alerts sections  
  
  if (i == 1) {
	  
	  WebUI.sendKeys(findTestObject('Object Repository/Sign_In/Email'), findTestData('Data file/Mubasher data').getValue(
			  1, 1))
	  
	  WebUI.sendKeys(findTestObject('Object Repository/Sign_In/Password'), findTestData('Data file/Mubasher data').getValue(
		  2, 1))
	  
	  WebUI.click(findTestObject("Sign_In/SignIn Button"))
	  
	  WebUI.doubleClick(findTestObject("Personal/Alerts/My Alerts"))
	  
	  WebUI.doubleClick(findTestObject("Personal/Alerts/Alerts"))
	  
	  WebUI.doubleClick(findTestObject("Personal/Alerts/Cteate an alets"))
	  
  }
  
  //Newsletter > Select your preferred exchanges section 
  
	  if(i == 3){
		  //WebUI.scrollToElement(findTestObject('Page_CuraHomepage/btn_MakeAppointment'), 3
		  //WebUI.scrollToElement(findTestObject("Personal/Newsletter/Market check box"),GlobalVariable.load_time,FailureHandling.CONTINUE_ON_FAILURE)
		  WebUI.check(findTestObject("Personal/Newsletter/Market check box"),FailureHandling.CONTINUE_ON_FAILURE)
		  WebUI.uncheck(findTestObject("Personal/Newsletter/Market check box"),FailureHandling.CONTINUE_ON_FAILURE)
		  //WebUI.scrollToElement(findTestObject("Personal/Newsletter/Save Button"),GlobalVariable.load_time,FailureHandling.CONTINUE_ON_FAILURE)
		  WebUI.doubleClick(findTestObject("Personal/Newsletter/Save Button"))
		  
		  
	  }
  
  
}


// 22/\05\2019 -lahiru Madhawa 
