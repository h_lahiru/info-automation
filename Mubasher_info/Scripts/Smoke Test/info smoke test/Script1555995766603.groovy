import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

CustomKeywords.'mubasher.help_keyword_start.gettingstart'()

//header navigation 

for(int i =1;i<=11;i++){

CustomKeywords.'mubasher.header__navigation.getHeadernavMenuTestObjectItem'(Integer.toString(i))

}

//refresh the whole page 
WebUI.refresh()


//navigation to market section 


for (int i =1;i<=5;i++) {
	//navigate to market 1st columns
	CustomKeywords.'mubasher.header__navigation.getHeadernavMenuTestObjectItem'('2')
	CustomKeywords.'topmenu.marketsNav.navigatetoSubMenuItemMarkets'('1', Integer.toString(i))
		
}

for(int i =1;i<=4;i++){
	//navigate to market 2nd columns
	CustomKeywords.'mubasher.header__navigation.getHeadernavMenuTestObjectItem'('2')
	CustomKeywords.'topmenu.marketsNav.navigatetoSubMenuItemMarkets'('2', Integer.toString(i))
}

for(int i =1;i<=4;i++){
	//navigate to market 3rd columns
	CustomKeywords.'mubasher.header__navigation.getHeadernavMenuTestObjectItem'('2')
	CustomKeywords.'topmenu.marketsNav.navigatetoSubMenuItemMarkets'('3', Integer.toString(i))
}

for(int i =1;i<=3;i++){
	//navigate to market 4th columns
	CustomKeywords.'mubasher.header__navigation.getHeadernavMenuTestObjectItem'('2')
	CustomKeywords.'topmenu.marketsNav.navigatetoSubMenuItemMarkets'('4', Integer.toString(i))
}


//navigation to news section

	for(int i =1;i<=7;i++){
		//navigate to news 1st column
		CustomKeywords.'mubasher.header__navigation.getHeadernavMenuTestObjectItem'('3')
		CustomKeywords.'topmenu.NewsNav.navigatetoSubMenuItemNews'('1','1',Integer.toString(i))
	}
	
	for(int i =1;i<=4;i++){
		//navigate to news 2nd column 1 ui
		CustomKeywords.'mubasher.header__navigation.getHeadernavMenuTestObjectItem'('3')
		CustomKeywords.'topmenu.NewsNav.navigatetoSubMenuItemNews'('2','1',Integer.toString(i))
	}
	
	for(int i =1;i<=2;i++){
		//navigate to market 4th columnsnews 2nd column 2 ui
		CustomKeywords.'mubasher.header__navigation.getHeadernavMenuTestObjectItem'('3')
		CustomKeywords.'topmenu.NewsNav.navigatetoSubMenuItemNews'('2','2',Integer.toString(i))
	}
	
	for(int i =1;i<=6;i++){
		//navigate to news 3rd column
		CustomKeywords.'mubasher.header__navigation.getHeadernavMenuTestObjectItem'('3')
		CustomKeywords.'topmenu.NewsNav.navigatetoSubMenuItemNews'('3','1',Integer.toString(i))
	}
	
	for(int i =1;i<=6;i++){
		//navigate to news 4th column
		CustomKeywords.'mubasher.header__navigation.getHeadernavMenuTestObjectItem'('3')
		CustomKeywords.'topmenu.NewsNav.navigatetoSubMenuItemNews'('4','1',Integer.toString(i))
	}
	
	for(int i =1;i<=4;i++){
		//navigate to news 5th column
		CustomKeywords.'mubasher.header__navigation.getHeadernavMenuTestObjectItem'('3')
		CustomKeywords.'topmenu.NewsNav.navigatetoSubMenuItemNews'('5','1',Integer.toString(i))
	}
	
	for(int i =1;i<=6;i++){
		//navigate to news 6th columns
		CustomKeywords.'mubasher.header__navigation.getHeadernavMenuTestObjectItem'('3')
		CustomKeywords.'topmenu.NewsNav.navigatetoSubMenuItemNews'('6','1',Integer.toString(i))
	}
	
	//navigation to Companies section
	
			for(int i =1;i<=4;i++){
				//navigate to news 1st column
				CustomKeywords.'mubasher.header__navigation.getHeadernavMenuTestObjectItem'('4')
				CustomKeywords.'topmenu.companiesNav.navigatetoSubMenuItemCompanies'('1', Integer.toString(i))
			}
			
			for(int i =1;i<=4;i++){
				//navigate to news 2nd column
				CustomKeywords.'mubasher.header__navigation.getHeadernavMenuTestObjectItem'('4')
				CustomKeywords.'topmenu.companiesNav.navigatetoSubMenuItemCompanies'('2', Integer.toString(i))
			}
			
			for(int i =1;i<=4;i++){
				//navigate to news 3rd column
				if (i==2) { continue; }//ipo screener is not working properly -
				CustomKeywords.'mubasher.header__navigation.getHeadernavMenuTestObjectItem'('4')
				CustomKeywords.'topmenu.companiesNav.navigatetoSubMenuItemCompanies'('3', Integer.toString(i))
			}
			
	//navigation to funds section
	
	for(int i =1;i<=2;i++){
		//navigate to news 1st row
		CustomKeywords.'mubasher.header__navigation.getHeadernavMenuTestObjectItem'('5')
		CustomKeywords.'topmenu.fundsNav.navigatetoSubMenuItemfunds'(Integer.toString(i), '1')
	}
	
	
//navigation to analysis tools section

			for(int i =1;i<=3;i++){
				//navigate to news 1st column
				CustomKeywords.'mubasher.header__navigation.getHeadernavMenuTestObjectItem'('6')
				CustomKeywords.'topmenu.AnalysisNav.navigatetoSubMenuItemAnalysis'('1', Integer.toString(i))
			}
				
			for(int i =1;i<=3;i++){
				//navigate to news 2nd column
				CustomKeywords.'mubasher.header__navigation.getHeadernavMenuTestObjectItem'('6')
				CustomKeywords.'topmenu.AnalysisNav.navigatetoSubMenuItemAnalysis'('2', Integer.toString(i))
			}
			
			
//navigation to fixed income section
			
			for(int i =1;i<=3;i++){
				//navigate to news 1st column
				CustomKeywords.'mubasher.header__navigation.getHeadernavMenuTestObjectItem'('9')
				CustomKeywords.'topmenu.fixedincomeNav.navigatetoSubMenuItemFixedincome'('1', Integer.toString(i))
			}
		
			for(int i =1;i<=3;i++){
				//navigate to news 2nd column
				
				if (i==2 || i==3) { continue; }//League table is not working properly -
				CustomKeywords.'mubasher.header__navigation.getHeadernavMenuTestObjectItem'('9')
				CustomKeywords.'topmenu.fixedincomeNav.navigatetoSubMenuItemFixedincome'('2', Integer.toString(i))
			}		
			
//navigation to personal section
			
				for(int i =1;i<=3;i++){
					//navigate to news 1st column
					
					CustomKeywords.'mubasher.header__navigation.getHeadernavMenuTestObjectItem'('10')
					CustomKeywords.'topmenu.personalNav.navigatetoSubMenuItempersonalNav'(Integer.toString(i))
				}
				
//03-05-2019 Lahiru