import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import javax.wsdl.Import

import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

import groovy.time.TimeCategory

import java.time.format.DateTimeFormatter;
import java.time.LocalDate
import java.time.LocalDateTime;
import java.awt.print.Printable
import java.text.SimpleDateFormat;
import java.util.Date;
import com.kms.katalon.core.logging.KeywordLogger

CustomKeywords.'mubasher.help_keyword_start.gettingstart'()
	

ArrayList<String> test = new ArrayList<String>(); // Create an ArrayList object
String marketName = "";
String marketName1 = "";
String marketname2 = "";
String marketname3 = "";

	for (int i =1;i<=5;i++) {
		
		//navigate to market 1st columns
		CustomKeywords.'mubasher.header__navigation.getHeadernavMenuTestObjectItem'('2')
		CustomKeywords.'topmenu.marketsNav.navigatetoSubMenuItemMarkets'('1', Integer.toString(i))
		
		//Market name 
		String Market_Name = WebUI.getText(findTestObject("Market/Market Name"))
		
		//coping the last updated date from web page
		String dateTime = WebUI.getText(findTestObject("Market/DateTime"))
	
		//split till Last update: "day", 
		//dateTime=dateTime.split(":")[1];
		dateTime=dateTime.split(", ")[1];
		println dateTime
		
		
//		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMMM dd");
//		LocalDate dateTime2 = LocalDate.parse(dateTime, formatter);
//		println dateTime2
		
		
		//getting system date MMM DD Format
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MMMM dd");//yyyy/MM/dd HH:mm:ss
		LocalDateTime now = LocalDateTime.now();
		todayDate=dtf.format(now);
		println todayDate
		
		//matching those date format as a string
	
		String x;
		if (dateTime == todayDate){
			
			x =""
			
		}else{
		
		x = "<<<<< check this out "
		}
		

		marketName=marketName+Market_Name+' || Last Update date: ';
		marketName=marketName+dateTime+'|| Current Date: ';
		marketName=marketName+todayDate+ '  ';
		marketName=marketName+x+'\n ';
	 	
		// WebUI.verifyMatch(dateTime, date1, true , FailureHandling.CONTINUE_ON_FAILURE) == true
	}
	
	
	for(int i =1;i<=4;i++){
		
		//navigate to market 2nd columns
		CustomKeywords.'mubasher.header__navigation.getHeadernavMenuTestObjectItem'('2')
		CustomKeywords.'topmenu.marketsNav.navigatetoSubMenuItemMarkets'('2', Integer.toString(i))
		
		//Market name 
		String Market_Name = WebUI.getText(findTestObject("Market/Market Name"))
		
		//coping the last updated date from web page
		String dateTime = WebUI.getText(findTestObject("Market/DateTime"))
	
		//split till Last update: "day", 
		//dateTime=dateTime.split(":")[1];
		dateTime=dateTime.split(", ")[1];
		println dateTime
		
		
//		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMMM dd");
//		LocalDate dateTime2 = LocalDate.parse(dateTime, formatter);
//		println dateTime2
		
		
		//getting system date MMM DD Format
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MMMM dd");//yyyy/MM/dd HH:mm:ss
		LocalDateTime now = LocalDateTime.now();
		todayDate=dtf.format(now);
		println todayDate
		
		//matching those date format as a string
	
		String x;
		if (dateTime == todayDate){
			
			x =""
			
		}else{
		
		x = "<<<<< check this out "
		}
		

		marketName=marketName+Market_Name+' || Last Update date: ';
		marketName=marketName+dateTime+'|| Current Date: ';
		marketName=marketName+todayDate+ '  ';
		marketName=marketName+x+'\n ';
	 	
		// WebUI.verifyMatch(dateTime, date1, true , FailureHandling.CONTINUE_ON_FAILURE) == true
	}
	
	
	for(int i =1;i<=4;i++){
		
		//if(i ==1 || i==2){continue;}
		
		//navigate to market 3rd columns
		CustomKeywords.'mubasher.header__navigation.getHeadernavMenuTestObjectItem'('2')
		CustomKeywords.'topmenu.marketsNav.navigatetoSubMenuItemMarkets'('3', Integer.toString(i))
		
		//Market name 
		String Market_Name = WebUI.getText(findTestObject("Market/Market Name"))
		
		//coping the last updated date from web page
		String dateTime = WebUI.getText(findTestObject("Market/DateTime"))
	
		//split till Last update: "day", 
		//dateTime=dateTime.split(":")[1];
		dateTime=dateTime.split(", ")[1];
		println dateTime
		
		
//		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMMM dd");
//		LocalDate dateTime2 = LocalDate.parse(dateTime, formatter);
//		println dateTime2
		
		
		//getting system date MMM DD Format
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MMMM dd");//yyyy/MM/dd HH:mm:ss
		LocalDateTime now = LocalDateTime.now();
		todayDate=dtf.format(now);
		println todayDate
		
		//matching those date format as a string
	
		String x;
		if (dateTime == todayDate){
			
			x =""
			
		}else{
		
		x = "<<<<< check this out "
		}
		

		marketName=marketName+Market_Name+' || Last Update date: ';
		marketName=marketName+dateTime+'|| Current Date: ';
		marketName=marketName+todayDate+ '  ';
		marketName=marketName+x+'\n ';
	 	
		// WebUI.verifyMatch(dateTime, date1, true , FailureHandling.CONTINUE_ON_FAILURE) == true
		
	}
	
	
	println (marketName + marketName1 + marketname2 + marketname3);
	KeywordLogger log = new KeywordLogger()
	log.logInfo(marketName + marketName1 + marketname2 + marketname3)
		//Lahiru Madhawa - 08/05/2019
