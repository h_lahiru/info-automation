import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.logging.KeywordLogger
KeywordLogger log = new KeywordLogger()

//loging into the mubasher web app
CustomKeywords.'mubasher.help_keyword_start.gettingstart'()

//navigate to login button
WebUI.click(findTestObject('header__top-for-desktop/login button'))

//click on Register
WebUI.click(findTestObject('Sign Up/Register'))

//random mail generating and send it to the text field
'random mail generator\r\n'
String mail = CustomKeywords.'mubasher.help_keyword_RandomEmail.getEmail'('opps', 'mailinator.com')

WebUI.sendKeys(findTestObject('Sign Up/email'), mail)

//email is now generating randomly hence commented
//WebUI.sendKeys(findTestObject('Sign Up/email'), findTestData('Data file/Mubasher data').getValue(3, 1))
//getting un/pw/first/name  from a excel sheet
WebUI.sendKeys(findTestObject('Sign Up/password'), findTestData('Data file/Mubasher data').getValue(4, 1))

WebUI.sendKeys(findTestObject('Sign Up/confirm password'), findTestData('Data file/Mubasher data').getValue(4, 1))

WebUI.sendKeys(findTestObject('Sign Up/name'), findTestData('Data file/Mubasher data').getValue(5, 1))

//click on sign up
WebUI.click(findTestObject('Sign Up/Sign up button'))

//logic for Email already exists or Registration Successful function.

if (WebUI.verifyTextPresent('Email already exists', false, FailureHandling.OPTIONAL)) {
	
    println('Email already exists')
	log.logInfo("Email already exists")
	
} else {

    WebUI.verifyTextPresent('Registration Successful', false)
}

println mail;
log.logInfo(mail)

//lahiru 03/04/2019 