import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

CustomKeywords.'mubasher.help_keyword_start.gettingstart'()

String Credential1 = WebUI.getText(findTestObject('Object Repository/header__columns/Market'))

////CustomKeywords.‘myPack.WriteExcel.demoKey’(Credential1,‘Credential1’)

CustomKeywords.'trail.WriteExcel.demoKey'(Credential1, 'Credential1')

System.out.println(Credential1) //Output will be “Enter your details below”

////WebUI.scrollToElement(findTestObject(‘Object Repository/SampleProjects/Katalon_DDF/Page_About Us Katalon Studio_DDF_READ_Write/a_Company’,5))
//
//WebUI.click(findTestObject('Object Repository/SampleProjects/Katalon_DDF/Page_About Us Katalon Studio_DDF_READ_Write/a_Company'))

String Credential2 = WebUI.getText(findTestObject('header__columns/dropdown'))

CustomKeywords.'trail.WriteExcel.demoKey'(Credential2, 'Credential2')

System.out.println(Credential2)//Output will be 3K