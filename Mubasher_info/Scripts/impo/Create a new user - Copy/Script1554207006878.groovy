import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

for (row = 1; row <= findTestData('Data file/Mubasher data').getRowNumbers(); row++)
{
WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.stagingurl)

WebUI.maximizeWindow()


//navigate to login button
WebUI.click(findTestObject('header__top-for-desktop/login button'))

//click on Register
WebUI.click(findTestObject('Sign Up/Register'))

//getting un/pw/first/name  from a excel sheet
WebUI.sendKeys(findTestObject('Sign Up/email'), findTestData('Data file/Mubasher data').getValue(3, 1))

WebUI.sendKeys(findTestObject('Sign Up/password'), findTestData('Data file/Mubasher data').getValue(4, 1))

WebUI.sendKeys(findTestObject('Sign Up/confirm password'), findTestData('Data file/Mubasher data').getValue(4, 1))

WebUI.sendKeys(findTestObject('Sign Up/name'), findTestData('Data file/Mubasher data').getValue(5, 1))

//click on sign up
WebUI.click(findTestObject('Sign Up/Sign up button'))

//logic for Email already exists or Registration Successful function

if(WebUI.verifyTextPresent('Email already exists', false, FailureHandling.OPTIONAL)){
	
	println 'Email already exists'
	
}else{

WebUI.verifyTextPresent('Registration Successful', false)

WebUI.waitForPageLoad(30)

}

}
	
//lahiru 02/04/2019

