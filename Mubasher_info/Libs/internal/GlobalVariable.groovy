package internal

import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.main.TestCaseMain


/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */
public class GlobalVariable {
     
    /**
     * <p>Profile Mubasher : Base url</p>
     */
    public static Object URl
     
    /**
     * <p>Profile Mubasher : test url</p>
     */
    public static Object stagingurl
     
    /**
     * <p>Profile Mubasher : loading time</p>
     */
    public static Object load_time
     
    /**
     * <p>Profile Mubasher : time 1</p>
     */
    public static Object time_1
     
    /**
     * <p>Profile Mubasher : base url english</p>
     */
    public static Object url1
     

    static {
        try {
            def selectedVariables = TestCaseMain.getGlobalVariables("default")
			selectedVariables += TestCaseMain.getGlobalVariables(RunConfiguration.getExecutionProfile())
            selectedVariables += RunConfiguration.getOverridingParameters()
    
            URl = selectedVariables['URl']
            stagingurl = selectedVariables['stagingurl']
            load_time = selectedVariables['load_time']
            time_1 = selectedVariables['time_1']
            url1 = selectedVariables['url1']
            
        } catch (Exception e) {
            TestCaseMain.logGlobalVariableError(e)
        }
    }
}
