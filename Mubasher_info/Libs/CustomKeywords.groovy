
/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */

import java.lang.String


def static "date_time.currentdatetime.main"(
    	String[] args	) {
    (new date_time.currentdatetime()).main(
        	args)
}

def static "date_time.currentdatetime.getTodayDate"() {
    (new date_time.currentdatetime()).getTodayDate()
}

def static "topmenu.NewsNav.navigatetoSubMenuItemNews"(
    	String menuid1	
     , 	String menuid2	
     , 	String menuid3	) {
    (new topmenu.NewsNav()).navigatetoSubMenuItemNews(
        	menuid1
         , 	menuid2
         , 	menuid3)
}

def static "trail.copyinnotepad.main1"(
    	String[] args	) {
    (new trail.copyinnotepad()).main1(
        	args)
}

def static "mubasher.help_keyword_RandomEmail.getEmail"(
    	String suffix	
     , 	String prefix	) {
    (new mubasher.help_keyword_RandomEmail()).getEmail(
        	suffix
         , 	prefix)
}

def static "topmenu.fixedincomeNav.navigatetoSubMenuItemFixedincome"(
    	String menuid1	
     , 	String menuid2	) {
    (new topmenu.fixedincomeNav()).navigatetoSubMenuItemFixedincome(
        	menuid1
         , 	menuid2)
}

def static "topmenu.AnalysisNav.navigatetoSubMenuItemAnalysis"(
    	String menuid1	
     , 	String menuid2	) {
    (new topmenu.AnalysisNav()).navigatetoSubMenuItemAnalysis(
        	menuid1
         , 	menuid2)
}

def static "com.database.database.connectDB"(
    	String url	
     , 	String dbname	
     , 	String port	
     , 	String username	
     , 	String password	) {
    (new com.database.database()).connectDB(
        	url
         , 	dbname
         , 	port
         , 	username
         , 	password)
}

def static "com.database.database.executeQuery"(
    	String queryString	) {
    (new com.database.database()).executeQuery(
        	queryString)
}

def static "com.database.database.closeDatabaseConnection"() {
    (new com.database.database()).closeDatabaseConnection()
}

def static "com.database.database.execute"(
    	String queryString	) {
    (new com.database.database()).execute(
        	queryString)
}

def static "topmenu.fundsNav.navigatetoSubMenuItemfunds"(
    	String menuid1	
     , 	String menuid2	) {
    (new topmenu.fundsNav()).navigatetoSubMenuItemfunds(
        	menuid1
         , 	menuid2)
}

def static "topmenu.personalNav.navigatetoSubMenuItempersonalNav"(
    	String menuid1	) {
    (new topmenu.personalNav()).navigatetoSubMenuItempersonalNav(
        	menuid1)
}

def static "trail.testingone.navigatetoMainMenuItem"(
    	String menuid	) {
    (new trail.testingone()).navigatetoMainMenuItem(
        	menuid)
}

def static "mubasher.market_selector.navigatetoMaindropMenuItem"(
    	String dropID	) {
    (new mubasher.market_selector()).navigatetoMaindropMenuItem(
        	dropID)
}

def static "topmenu.companiesNav.navigatetoSubMenuItemCompanies"(
    	String menuid1	
     , 	String menuid2	) {
    (new topmenu.companiesNav()).navigatetoSubMenuItemCompanies(
        	menuid1
         , 	menuid2)
}

def static "mubasher.header__navigation.getHeadernavMenuTestObjectItem"(
    	String nav_id	) {
    (new mubasher.header__navigation()).getHeadernavMenuTestObjectItem(
        	nav_id)
}

def static "topmenu.marketsNav.navigatetoSubMenuItemMarkets"(
    	String menuid1	
     , 	String menuid2	) {
    (new topmenu.marketsNav()).navigatetoSubMenuItemMarkets(
        	menuid1
         , 	menuid2)
}

def static "mubasher.help_keyword_start.gettingstart"() {
    (new mubasher.help_keyword_start()).gettingstart()
}
